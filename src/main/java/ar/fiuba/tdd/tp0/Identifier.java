package ar.fiuba.tdd.tp0;


import java.util.*;


/**
 * Created by pc on 5/09/15.
 */
public class Identifier {
        
        Sum sum = new Sum();
        Substract substract = new Substract();
        Multiply multiply = new Multiply();
        Division division = new Division();
        Rest rest = new Rest();
        SumN sumN = new SumN();
        SubstractN substractN = new SubstractN();
        DivisionN divisionN = new DivisionN();
        MultiplyN multiplyN = new MultiplyN();
        Constant constant = new Constant();

    public Calculable identify(String sign){
            HashMap<String,String> tabla1 = new HashMap<String,String>();
            tabla1.put("+","+");
            tabla1.put("-","-");
            tabla1.put("*","*");
            tabla1.put("/","/");
            tabla1.put("MOD","MOD");
            tabla1.put("++","++");
            tabla1.put("--","--");
            tabla1.put("**","**");
            tabla1.put("//","//");



            HashMap<String,Calculable> tabla2 = new HashMap<String,Calculable>();
            tabla2.put("+", this.sum);
            tabla2.put("-", this.substract);
            tabla2.put("*", this.multiply);
            tabla2.put("/", this.division);
            tabla2.put("MOD", this.rest);
            tabla2.put("++", this.sumN);
            tabla2.put("--", this.substractN);
            tabla2.put("**", this.multiplyN);
            tabla2.put("//", this.divisionN);
            tabla2.put(null, this.constant);

            return tabla2.get(tabla1.get(sign));
    }
}
