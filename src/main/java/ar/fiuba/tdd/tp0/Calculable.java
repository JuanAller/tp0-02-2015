package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 5/09/15.
 */
public interface Calculable {

    StackHandler calculate(StackHandler stackHandler, String operator);

}
