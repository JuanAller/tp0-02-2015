package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 5/09/15.
 */
public class Multiply implements Calculable {

    @Override
    public StackHandler calculate(StackHandler stackHandler, String operator) {


        Float[] array = stackHandler.getParameters();
        Float result = array[0] * array[1];
        stackHandler.keepValue(result);
        return stackHandler;

    }

}
