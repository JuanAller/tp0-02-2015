package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 5/09/15.
 */
public class SubstractN implements Calculable{
    @Override
    public StackHandler calculate(StackHandler stackHandler, String operator) {

        Stack<Float> stack = stackHandler.getParametersN();
        Float multisubstract = stack.pop();
        while ( stack.size() != 0){
            multisubstract = multisubstract - stack.pop();
        }
        stackHandler.keepValue(multisubstract);
        return stackHandler;
    }


}
