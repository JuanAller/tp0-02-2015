package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 5/09/15.
 */
public class SumN implements Calculable{


    @Override
    public StackHandler calculate(StackHandler stackHandler, String operator) {

        Stack<Float> stack = stackHandler.getParametersN();
        Sum sum = new Sum();
        while (stack.size() != 1) {
            stackHandler = sum.calculate(stackHandler,"+");
        }
        return stackHandler;
    }



}
