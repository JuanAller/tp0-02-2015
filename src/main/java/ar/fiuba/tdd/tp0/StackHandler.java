package ar.fiuba.tdd.tp0;
import java.util.Stack;
/**
 * Created by pc on 10/09/15.
 */
public class StackHandler {
    Stack<Float> stack = new Stack<Float>();

    public void checkParametersN(){
        while (this.stack.empty()){
            throw new IllegalArgumentException();
        }
    }
    public void checkParameters(){
        while (this.stack.size() <= 1){
            throw new IllegalArgumentException();
        }

    }
    private void investStack(){
        Stack<Float> assistance = new Stack<>();
        while (!this.stack.empty()){
            assistance.push(this.stack.pop());
        }
        this.stack = assistance;
    }
    public Stack<Float> getParametersN(){
        this.checkParametersN();
        this.investStack();

        return this.stack;

    }
    public Float[] getParameters(){
        this.checkParameters();
        Float[] array = new Float[2];
        array[0] = this.stack.pop();
        array[1] = this.stack.pop();
        return array;
    }
    public void keepValue(Float value){
        this.stack.push(value);
    }
    public Float getValue(){
        return this.stack.pop();
    }
}
