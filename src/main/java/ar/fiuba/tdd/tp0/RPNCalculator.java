package ar.fiuba.tdd.tp0;

import java.util.Objects;
import java.util.Stack;

public class RPNCalculator {

    public float eval(String expression) {

        try{
            Objects.requireNonNull(expression);
        }
        catch (NullPointerException e){
            throw new IllegalArgumentException();
        }
        StackHandler stackHandler = new StackHandler();
        Identifier id = new Identifier();
        String[] operators = expression.split(" ");
        for (int i = 0 ; i < operators.length ; i++) {
            Calculable operator = id.identify(operators[i]);
            stackHandler = operator.calculate(stackHandler,operators[i]);
        }
        return stackHandler.getValue();
    }

}
