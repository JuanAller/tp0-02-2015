package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 7/09/15.
 */
public class Constant implements Calculable{

    @Override
    public StackHandler calculate(StackHandler stackHandler, String operator) {

        Float number = Float.parseFloat(operator);
        stackHandler.keepValue(number);
        return stackHandler;
    }



}
