package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 5/09/15.
 */
public class MultiplyN implements Calculable {

    @Override
    public StackHandler calculate(StackHandler stackHandler, String operator) {

        Stack<Float> stack = stackHandler.getParametersN();
        Multiply multiply = new Multiply();
        while (stack.size() != 1){
            multiply.calculate(stackHandler,"*");
        }
        return stackHandler;
    }

}
