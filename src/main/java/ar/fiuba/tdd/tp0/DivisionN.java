package ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by pc on 5/09/15.
 */
public class DivisionN implements Calculable {
    @Override
    public StackHandler calculate(StackHandler stackHandler, String operator) {

        Stack<Float> stack = stackHandler.getParametersN();
        Float multidivision = stack.pop();
        while ( !stack.empty()){
            multidivision = multidivision / stack.pop();
        }
        stackHandler.keepValue(multidivision);
        return stackHandler;
        }


}
